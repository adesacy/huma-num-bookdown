# Huma-Num bookdown

Ce repository a vocation à permettre à des projets en SHS de publier leurs recherches et leurs traitements de données en format _bookdown_ (https://github.com/rstudio/bookdown).
Les fichiers de configuration ont déjà été configurés de façon à ce que le site soit publiable directement via la fonctionnalité *GitLabPages*.

## Utilisation

- Récupérer le contenu du repository Gitlab en le clonant sur votre ordinateur : `git clone https://gitlab.huma-num.fr/adesacy/huma-num-bookdown.git`.
- Créer votre propre repository GitLab qui accueillera votre projet.
- Effectuez votre premier _push_ d'initialisation du repository. Lors de ce premier _push_, le pipeline d'intégration continue permettant la création du site s'effectue automatiquement à partir du fichier de configuration `.gitlab-ci.yml`. Vous pouvez suivre ce pipeline et son avancée : 
  - Onglet *Build* > *Pipeline*.
- Vous pouvez maintenant passer à l'étape de déploiement effectif du site en ligne grâce à Pages :
  - Rendez-vous dans l'onglet *Deploy* > *Pages* > *run pipeline*
- Votre site est maintenant déployée à l'adresse suivante : "https://" + "nom_utilisateur_gitlab." + "gitpages.huma-num.fr/" + "nom_du_projet". Ex : https://adesacy.gitpages.huma-num.fr/huma-num-bookdown/.
- Chaque _push_ relancera le pipeline de déploiement du site. Pour limiter cela, n'hésitez pas à travailler sur des branches parallèles et à _pusher_ sur _main_ lorsque vous voulez mettre à jour votre site web - cela limitera au passage la charge du serveur GitLab.

## Informations : 

Ces informations sont extraites et traduites du livre de Yihui Xie, _R Markdown The Definitive Guide_, Ch. 12, https://bookdown.org/yihui/rmarkdown/bookdown-project.html.

La structure de base d'un projet **bookdown** par défaut est présentée ci-dessous :

```markdown
directory/
├──  index.Rmd
├── 01-intro.Rmd
├── 02-literature.Rmd
├── 03-method.Rmd
├── 04-application.Rmd
├── 05-summary.Rmd
├── 06-references.Rmd
├── _bookdown.yml
├── _output.yml
├──  book.bib
├──  preamble.tex
├──  README.md
└──  style.css
```

Voici un résumé de ces fichiers :

- `index.Rmd` : C'est le seul document Rmd à contenir un préambule YAML tel que décrit dans le chapitre 2. Ce fichier index constitue le premier chapitre du livre.

- Fichiers .Rmd : Un livre **bookdown** typique contient plusieurs chapitres, et chaque chapitre est rédigé dans un fichier Rmd séparé (01-intro.Rmd ; 02-literature.Rmd ; etc.).

- `_bookdown.yml` : Un fichier de configuration pour **bookdown**.

- `_output.yml` : Il spécifie le formatage du HTML, du LaTeX/PDF et des livres électroniques.

- `preamble.tex` et `style.css` : Ils peuvent être utilisés pour ajuster l'apparence et les styles des documents de sortie du livre. Des connaissances en LaTeX et/ou CSS sont nécessaires.

### Fichier d'index

Le fichier `index.Rmd` contient le premier chapitre et les métadonnées YAML, par exemple,

```yaml
---
title: "A Minimal Book Example"
author: "Yihui Xie"
date: "`r "\x60r Sys.Date()\x60"`"
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
description: "This is a minimal example of using
  the bookdown package to write a book."
---
```

### Rmd files

Par défaut, tous les fichiers Rmd sont fusionnés avec le fichier `index.Rmd` pour générer le livre. Les fichiers Rmd doivent commencer immédiatement par le titre du chapitre en utilisant l'en-tête de premier niveau, par exemple, `# Titre du chapitre`. Notez que les métadonnées YAML ne doivent pas être incluses dans ces fichiers Rmd, car elles sont héritées du fichier `index.Rmd`. 

- 01-intro.Rmd

    ```markdown
    # Introduction

    This chapter is an overview of the methods that
    we propose to solve an **important problem**.
    ```

- 02-literature.Rmd

    ```markdown
    # Literature

    Here is a review of existing methods.
    ```

Par défaut, **bookdown** fusionne tous les fichiers Rmd dans l'ordre des noms de fichiers, par exemple, `01-intro.Rmd` apparaîtra avant `02-literature.Rmd`. Les noms de fichiers qui commencent par un trait de soulignement `_` sont ignorés.

### `_bookdown.yml`

Le fichier `_bookdown.yml` vous permet de spécifier des paramètres optionnels pour construire le livre. Par exemple, vous pouvez vouloir modifier l'ordre dans lequel les fichiers sont fusionnés en incluant le champ `rmd_files` :

```yaml
rmd_files: ["index.Rmd", "02-literature.Rmd", "01-intro.Rmd"]
```

### `_output.yml`

Le fichier `_output.yml` est utilisé pour spécifier les formats de sortie des livres (voir la section \@ref(bookdown-output)). Voici un bref exemple :

```yaml
bookdown::gitbook:
  lib_dir: assets
  split_by: section
  config:
    toolbar:
      position: static
bookdown::pdf_book:
  keep_tex: yes
bookdown::html_book:
  css: toc.css
```


## Références utiles : 

The **bookdown** book: https://bookdown.org/yihui/bookdown/.

The **bookdown** package reference site: https://pkgs.rstudio.com/bookdown.

- Xie, Yihui. 2016. _Bookdown: Authoring Books and Technical Documents with R Markdown_. Boca Raton, Florida: Chapman; Hall/CRC. https://bookdown.org/yihui/bookdown/.
- Barnier, Julien. 2022. Rmdformats: HTML Output Formats and Templates for Rmarkdown Documents. https://github.com/juba/rmdformats.
- Wickham, Hadley. 2015. _R Packages_. 1st ed. O’Reilly Media, Inc.
- Xie, Yihui. 2015. Dynamic Documents with R and Knitr. 2nd ed. Boca Raton, Florida: Chapman; Hall/CRC. https://yihui.name/knitr/.
- ———. 2022. Xaringan: Presentation Ninja. https://github.com/yihui/xaringan.
- ———. 2023a. Bookdown: Authoring Books and Technical Documents with r Markdown.
- ———. 2023b. Knitr: A General-Purpose Package for Dynamic Report Generation in R. https://yihui.org/knitr/.
